# README #

Dokumentation zu Hard- und Software des ca. 3 m langen PE-Rohres zur energieautarken Erfassung und Versendung von Bodenwerten von Acker- und Gartenbauflächen.

### Projektziel ###

Es soll ein Objekt entwickelt werden, welches

* über einen Erdbohrer ca. 2 m tief in den Boden eingebracht wird

* mittels des Seebeck-Effekts den internen Akku aufgeladen wird

* in verscheidenen Höhen bzw. Tiefen des Stabes beliebige Sensorwerte wie Erdfeuchte, Temperatur (für den passenden Aussaatzeitpunkt), Magnesiumgehalt. etc. erfassen

* und die Daten per LoRaWAN versenden soll.

Die genauen Anforderung sollen von Herrn Wido Beier kommen, der uns (Erik, Norbert) am 20.8. zusagte, mit zwei bis drei weiteren Bauern sowie seinem "Gärtner" zu sprechen und uns dann eine Rückmeldung zu geben.

### Hardware ###

Stand 21.8.2020

* Rohr
Die aktuelle Idee besteht darin, ein Rohr aus Polyethylen (kurz PE) wie aktuelle Wasserrohre, die man in der Erde verbaut, zu fertigen. Das Rohr ist dabei in der Länge zum besseren Einbau zwei geteilt. Eine Seite verfügt über Innengewinde, in das die Schraube kommt, mit der man die andere Hälfte nach Einbau aller Komponenten zusammen schraubt.

* SOC
Der einzige bisher am Markt verfügbare LoRaWAN-Chip kommt von STMicroelectronics: https://www.st.com/en/microcontrollers-microprocessors/stm32wl-series.html?icmp=tt13669_gl_pron_jan2020#products

* Sensoren
Bodenfeuchtigkeit:

Temperatur:

Wuchshöhe:

Bodenwerte:

* How to run tests
* Deployment instructions

### Software ###

* App-Oberfläche
OpenStreetMap-Karte mit Flurstücken und Bodenschätzungswerten (https://de.wikipedia.org/wiki/Bodensch%C3%A4tzung)

* Code review


* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact